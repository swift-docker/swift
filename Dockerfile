ARG BASE_IMAGE=registry.gitlab.com/swift-docker/swift/swiftenv:1.4.0
FROM $BASE_IMAGE
ARG SWIFT_NEW_VERSION=5.0
ARG SWIFT_OS=ubuntu2004
ARG OS=ubuntu20.04
RUN apt-get update && apt-get install jq -y
RUN swiftenv install https://download.swift.org/swift-${SWIFT_NEW_VERSION}-release/${SWIFT_OS}/swift-${SWIFT_NEW_VERSION}-RELEASE/swift-${SWIFT_NEW_VERSION}-RELEASE-${OS}.tar.gz
