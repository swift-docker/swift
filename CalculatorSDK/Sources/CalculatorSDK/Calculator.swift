import Foundation

public class Calculator {

    public class func add(a: Int, b: Int) -> Int {
        return a + b
    }

    public class func sub(a: Int, b: Int) -> Int {
        return add(a: a, b: -b)
    }

    public class func default_sub(a: Int, b: Int) -> Int {
        return a - b
    }

    public class func mul(a: Int, b: Int) -> Int {
        var mult = 0
        for _ in 0..<b {
            mult = add(a: mult, b: a)
        }
        return mult
    }

    public class func default_mul(a: Int, b: Int) -> Int {
        return a * b
    }

    public class func div(a: Int, b: Int) -> Int {
        var divi = a
        var counter = 0
        while divi > 0 {
            divi = sub(a: divi, b: b)
            if divi > 0 {
                counter = add(a: counter, b: 1)
            }
        }
        return counter
    }

    public class func default_div(a: Int, b: Int) -> Int {
        return a / b
    }

}
