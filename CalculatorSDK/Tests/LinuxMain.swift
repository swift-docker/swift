import XCTest

import CalculatorSDKTests

var tests = [XCTestCaseEntry]()
tests += CalculatorSDKTests.allTests()
XCTMain(tests)
