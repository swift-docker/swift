# Changelog

## 5.5

* docker pull registry.gitlab.com/therickys93/swift:5.5
* docker pull registry.gitlab.com/therickys93/swift:5.5.1
* docker pull registry.gitlab.com/therickys93/swift:5.5.2

## 5.4

* docker pull registry.gitlab.com/therickys93/swift:5.4
* docker pull registry.gitlab.com/therickys93/swift:5.4.1
* docker pull registry.gitlab.com/therickys93/swift:5.4.2
* docker pull registry.gitlab.com/therickys93/swift:5.4.3

## 5.3

* docker pull registry.gitlab.com/therickys93/swift:5.3
* docker pull registry.gitlab.com/therickys93/swift:5.3.1
* docker pull registry.gitlab.com/therickys93/swift:5.3.2
* docker pull registry.gitlab.com/therickys93/swift:5.3.3

## 5.2

* docker pull registry.gitlab.com/therickys93/swift:5.2.4
* docker pull registry.gitlab.com/therickys93/swift:5.2.5
